package com.shopcart.product.utils;

import com.shopcart.product.exceptions.BadRequestException;
import com.shopcart.product.models.ProductQuantityDTO;
import org.springframework.stereotype.Component;

import java.util.stream.StreamSupport;

@Component
public class Helpers {

    public static long getSize(Iterable data) {
       return StreamSupport.stream(data.spliterator() ,false).count();
    }

    public ProductQuantityDTO updateProductQuantity(String availableQuantity,String id , String quantity) {
        long new_quantity = Long.parseLong(availableQuantity) - Long.parseLong(quantity);
        if(new_quantity < 0)
            throw new BadRequestException("Requested Quantity is more than currently available quantity");
        return new ProductQuantityDTO(id , Long.toString(new_quantity));
    }
}
