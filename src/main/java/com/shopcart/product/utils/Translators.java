package com.shopcart.product.utils;

import com.shopcart.product.models.ProductDTO;
import com.shopcart.product.models.ProductResponseDTO;
import com.shopcart.product.repository.model.ProductDocument;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.stereotype.Component;

@Component
public class Translators {

    private final MapperFactory mapperFactory;

    private final MapperFacade mapperFacade;

    public Translators() {
        this.mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.classMap(ProductDocument.class, ProductResponseDTO.class).byDefault().register();
        mapperFactory.classMap(ProductDTO.class , ProductDocument.class).byDefault().register();
        mapperFacade = mapperFactory.getMapperFacade();
    }

    public ProductResponseDTO translateToProductDTOResponse (ProductDocument document ) {
        ProductResponseDTO productResponseDTO = mapperFacade.map(document, ProductResponseDTO.class);
        return productResponseDTO;
    }

    public ProductDocument translateToProductDocument(ProductDTO dto) {
        ProductDocument productDocument = mapperFacade.map(dto, ProductDocument.class);
        return productDocument;
    }
 }
