package com.shopcart.product.envelope;

import com.shopcart.product.models.ProductResponseDTO;
import common.shopcart.library.envelope.Envelope;

import java.util.List;

public class ProductResponseListDTOEnvelope extends Envelope<List<ProductResponseDTO>> {

    public ProductResponseListDTOEnvelope(List<ProductResponseDTO> payload) {
        super(payload);
    }
}
