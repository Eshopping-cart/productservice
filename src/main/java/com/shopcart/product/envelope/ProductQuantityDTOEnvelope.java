package com.shopcart.product.envelope;

import com.shopcart.product.models.ProductQuantityDTO;
import common.shopcart.library.envelope.Envelope;

public class ProductQuantityDTOEnvelope extends Envelope<ProductQuantityDTO> {

    public ProductQuantityDTOEnvelope(ProductQuantityDTO payload){
        super(payload);
    }
}
