package com.shopcart.product.envelope;

import com.shopcart.product.models.ProductResponseDTO;
import common.shopcart.library.envelope.Envelope;

public class ProductResponseDTOEnvelope extends Envelope<ProductResponseDTO> {

    public ProductResponseDTOEnvelope(ProductResponseDTO payload) {
        super(payload);
    }
}
