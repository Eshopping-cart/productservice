package com.shopcart.product.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shopcart.product.exceptions.BadRequestException;
import com.shopcart.product.exceptions.RecordNotFoundException;
import common.shopcart.library.envelope.Envelope;
import common.shopcart.library.envelope.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    @Override
    public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
        return (clientHttpResponse.getStatusCode().series() == CLIENT_ERROR ||
                clientHttpResponse.getStatusCode().series() == SERVER_ERROR
                );
    }

    @Override
    public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {
            HttpStatus httpStatus = clientHttpResponse.getStatusCode();
            String responseString = StreamUtils.copyToString(clientHttpResponse.getBody(), Charset.defaultCharset());
            ObjectMapper mapper = new ObjectMapper();
            Envelope envelope = mapper.readerFor(Envelope.class).readValue(responseString);
            ErrorResponse error = envelope.getError();
            switch (httpStatus) {
                case BAD_REQUEST:
                    throw new BadRequestException(error.getMessage());
                case NOT_FOUND:
                    throw new RecordNotFoundException(error.getMessage());
                default:
                    throw new HttpClientErrorException(clientHttpResponse.getStatusCode());
            }
    }
}
