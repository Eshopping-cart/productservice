package com.shopcart.product.clients;

import com.shopcart.product.models.ProductQuantityDTO;
import common.shopcart.library.envelope.Envelope;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static common.shopcart.library.util.ResponseEntityPayloadUtil.extractPayloadFromResponseEntity;

@Slf4j
@Component
public class WarehouseServiceAdapter {
	@Value("${warehouseService.url}")
	private String baseURL;

	private final RestTemplate restTemplate;

	@Autowired
	public WarehouseServiceAdapter(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public ProductQuantityDTO updateProductQuantity(String id, String quantity) {
		ProductQuantityDTO productQuantity = new ProductQuantityDTO();
		String newURL = baseURL + "/products/quantity/" + id;
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		productQuantity.setProductId(id);
		productQuantity.setQuantity(quantity);

		HttpEntity<ProductQuantityDTO>entity = new HttpEntity<ProductQuantityDTO>(productQuantity, headers);
		log.info("Calling Warehouse Service to update Product Quantity");
		ResponseEntity<Envelope> response = restTemplate.exchange(newURL, HttpMethod.PUT, entity, Envelope.class);
		return extractPayloadFromResponseEntity(response , new ProductQuantityDTO());
	}

	public ProductQuantityDTO addNewProduct(String id, String quantity) {
		ProductQuantityDTO productQuantity = new ProductQuantityDTO();
		HttpHeaders headers = new HttpHeaders();
		String NEW_URL = baseURL + "/products/quantity" + "/newProduct";
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		productQuantity.setProductId(id);
		productQuantity.setQuantity(quantity);

		HttpEntity<ProductQuantityDTO> entity = new HttpEntity<ProductQuantityDTO>(productQuantity, headers);
		log.info("Calling Warehouse Service to add new Product Quantity");
		ResponseEntity<Envelope> response = restTemplate.exchange(NEW_URL, HttpMethod.POST, entity, Envelope.class);
		return extractPayloadFromResponseEntity(response , new ProductQuantityDTO());
	}

	public ProductQuantityDTO getProductQuantityById(String id)  {
		HttpHeaders headers = new HttpHeaders();
		String NEW_URL = baseURL + "/products/quantity/" + id;
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<ProductQuantityDTO> entity = new HttpEntity<ProductQuantityDTO>(headers);
		log.info("Calling Warehouse Service to get Quantity for product with ProductId = {} " , id);
		ResponseEntity<Envelope> response = restTemplate.exchange(NEW_URL, HttpMethod.GET, entity, Envelope.class);
		//return response.getBody().getPayload();
		return extractPayloadFromResponseEntity(response , new ProductQuantityDTO());
	}
}
