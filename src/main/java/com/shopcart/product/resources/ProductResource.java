package com.shopcart.product.resources;

import com.shopcart.product.envelope.ProductQuantityDTOEnvelope;
import com.shopcart.product.envelope.ProductResponseDTOEnvelope;
import com.shopcart.product.envelope.ProductResponseListDTOEnvelope;
import com.shopcart.product.models.ProductDTO;
import com.shopcart.product.models.ProductQuantityDTO;
import com.shopcart.product.models.ProductResponseDTO;
import com.shopcart.product.models.enums.ProductType;
import com.shopcart.product.service.ProductService;
import common.shopcart.library.envelope.Envelope;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductResource {

	private final ProductService productService;

	@Autowired
	public ProductResource(ProductService productService) {
		this.productService = productService;
	}

	@ApiOperation(value = "Get list of all products", tags = "Product Service" , response = ProductResponseDTOEnvelope.class,
            produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping
	public ResponseEntity<Envelope> getProducts() {
		List<ProductResponseDTO> productResponseDTOList = productService.getProducts();
		return ResponseEntity.ok()
				.header("Customer-Header" ,"header")
				.body(new ProductResponseListDTOEnvelope(productResponseDTOList));
	}

	@ApiOperation(value = "Get product detail by ID", tags = "Product Service" , response = ProductResponseDTOEnvelope.class)
	@GetMapping("/{id}")
	public ResponseEntity<Envelope> getProduct(@PathVariable("id") String id) {
		ProductResponseDTO productResponseDTO =  productService.getProduct(id);
		return ResponseEntity.ok()
				.header("Customer-Header" ,"header")
				.body(new ProductResponseDTOEnvelope(productResponseDTO));
	}

	@ApiOperation(value = "Update product quantity ", response = ProductResponseDTOEnvelope.class, tags = "Product Service",
            produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping("/{id}")
	public ResponseEntity<Envelope> updateProduct(@PathVariable("id") String id, @RequestParam String quantity) {
		ProductQuantityDTO productQuantityDTO =  productService.updateProduct(id, quantity);
		return ResponseEntity.ok()
				.header("Customer-Header" ,"header")
				.body(new ProductQuantityDTOEnvelope(productQuantityDTO));
	}

	@ApiOperation(value = "Add a new Product", response = ProductResponseDTOEnvelope.class, tags = "Product Service",
            produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@PostMapping("/addProduct")
	public ResponseEntity<Envelope> addNewProduct(@RequestParam(value = "type") ProductType type , @RequestBody @Valid ProductDTO productDTO) {
		ProductResponseDTO productResponseDTO =  productService.addNewProduct(type ,productDTO);
		return ResponseEntity.ok()
				.header("Customer-Header" ,"header")
				.body(new ProductResponseDTOEnvelope(productResponseDTO));
	}

	@ApiOperation(value = "Get product Details by Name and Brand", response = ProductResponseDTOEnvelope.class, tags = "Product Service",
            produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping("/productDetails/{productName}/{brand}")
	public ResponseEntity<Envelope> getProductByNameAndBrand(@PathVariable("productName") String productName, @PathVariable("brand") String brand) {
        ProductResponseDTO productResponseDTO = productService.getProductByNameAndBrand(productName, brand);
        return ResponseEntity.ok()
                .header("Customer-Header" ,"header")
                .body(new ProductResponseDTOEnvelope(productResponseDTO));
	}

	@ApiOperation(value = "Get product Details by Type", response = ProductResponseDTOEnvelope.class, tags = "Product Service",
			produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping("/productDetails/{type}")
	public ResponseEntity<Envelope> getProductByType(@PathVariable("type") ProductType productType) {
		List<ProductResponseDTO> productResponseDTOList = productService.getProductByType(productType);
		return ResponseEntity.ok()
				.header("Customer-Header" ,"header")
				.body(new ProductResponseListDTOEnvelope(productResponseDTOList));
	}
}
