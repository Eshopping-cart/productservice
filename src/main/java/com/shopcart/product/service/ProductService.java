package com.shopcart.product.service;

import com.shopcart.product.exceptions.BadRequestException;
import com.shopcart.product.exceptions.RecordNotFoundException;
import com.shopcart.product.models.ProductDTO;
import com.shopcart.product.models.ProductQuantityDTO;
import com.shopcart.product.models.ProductResponseDTO;
import com.shopcart.product.models.enums.ProductType;
import com.shopcart.product.repository.ProductRepository;
import com.shopcart.product.repository.model.ProductDocument;
import com.shopcart.product.utils.Helpers;
import com.shopcart.product.utils.Translators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ProductService {

    private final ProductRepository repository;

    private final Translators translator;

    private final Helpers helpers;

    @Autowired
    public ProductService(ProductRepository repository, Translators translator, Helpers helpers) {
        this.repository = repository;
        this.translator = translator;
        this.helpers = helpers;
    }

    public List<ProductResponseDTO> getProducts() {
        Iterable<ProductDocument> productList =  repository.findAll();
        List<ProductResponseDTO> response = new ArrayList();
        productList.forEach(product -> response.add(translator.translateToProductDTOResponse(product)));
        return response;
    }

    public ProductResponseDTO getProduct(String id) {
        ProductDocument productDocument = repository.findById(id)
                .orElseThrow(() -> new RecordNotFoundException("Cannot find Product with productId = " +id));
        ProductResponseDTO response = translator.translateToProductDTOResponse(productDocument);
        return response;
    }

    public ProductResponseDTO addNewProduct(ProductType type ,ProductDTO productDTO) {
        String productName = productDTO.getProductName().toLowerCase();
        String brand = productDTO.getBrand().toLowerCase();
        Optional<ProductDocument> isProductAlreadyPresent = repository.findByProductNameAndBrand(
               productName,brand);
        if(isProductAlreadyPresent.isPresent()) {
            log.info("Product already present with productName = {} and brand = {},",productName , brand);
            throw new BadRequestException("Product with productName = " +productName + " and brand = " +brand + " already present");
        }
        productDTO.setProductName(productName);
        productDTO.setBrand(brand);
        ProductDocument productDocument = translator.translateToProductDocument(productDTO);
        productDocument.setType(type);
        productDocument = repository.save(productDocument);
        log.info("Successfully added a new Product with productId = {} to the database" , productDocument.getProductId());
        ProductResponseDTO response = translator.translateToProductDTOResponse(productDocument);
        return response;
    }

    public ProductResponseDTO getProductByNameAndBrand(String productName , String brand ){
        ProductDocument productDocument = repository.findByProductNameAndBrand(productName.toLowerCase() , brand.toLowerCase())
                .orElseThrow(() -> new RecordNotFoundException("Cannot find product with productName = " +productName +" and brand = " +brand));
        ProductResponseDTO response = translator.translateToProductDTOResponse(productDocument);
        return response;
    }

    public List<ProductResponseDTO> getProductByType(ProductType productType){
        Iterable<ProductDocument> productDocumentList = repository.findByType(productType);
        if(helpers.getSize(productDocumentList) == 0) {
            throw new RecordNotFoundException("Cannot find any product with productType = "+productType);
        }
        List<ProductResponseDTO> productResponseDTOList = new ArrayList();
        productDocumentList.forEach(productDocument ->
                productResponseDTOList.add(translator.translateToProductDTOResponse(productDocument)));
        return productResponseDTOList;
    }

    public ProductQuantityDTO updateProduct(String id , String quantity) {
        ProductDocument productDocument = repository.findById(id)
                .orElseThrow(() -> new BadRequestException("Cannot find product with productId = " +id));
        ProductQuantityDTO productQuantityDTO = helpers.updateProductQuantity(productDocument.getQuantity() ,id ,quantity);
        productDocument.setQuantity(productQuantityDTO.getQuantity());
        repository.save(productDocument);
        return productQuantityDTO;
    }
}
