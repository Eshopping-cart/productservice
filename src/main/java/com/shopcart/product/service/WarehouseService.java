package com.shopcart.product.service;

import com.shopcart.product.clients.WarehouseServiceAdapter;
import com.shopcart.product.models.ProductQuantityDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WarehouseService {

    private final WarehouseServiceAdapter warehouseServiceAdapter;

    @Autowired
    public WarehouseService(WarehouseServiceAdapter warehouseServiceAdapter) {
        this.warehouseServiceAdapter = warehouseServiceAdapter;
    }

    public ProductQuantityDTO updateProduct(String id, String quantity) {
        return warehouseServiceAdapter.updateProductQuantity(id, quantity);
    }

    public ProductQuantityDTO addNewProduct(String id , String quantity) {
        return warehouseServiceAdapter.addNewProduct(id , quantity);
    }

    public ProductQuantityDTO getProductQuantityById(String id) {
        return warehouseServiceAdapter.getProductQuantityById(id);
    }
}
