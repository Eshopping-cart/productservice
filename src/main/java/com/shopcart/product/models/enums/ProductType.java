package com.shopcart.product.models.enums;

public enum ProductType {
    GROCERY,
    CLOTHES,
    ELECTRONICS,
    HOME_ESSENTIALS,
    SPORTS,
}
