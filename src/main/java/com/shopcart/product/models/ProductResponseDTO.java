package com.shopcart.product.models;

import com.shopcart.product.models.enums.ProductType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProductResponseDTO extends ProductDTO {

    @NotNull
    private String productId;

    private ProductType type;
}
