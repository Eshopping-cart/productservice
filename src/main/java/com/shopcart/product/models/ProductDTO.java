package com.shopcart.product.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import common.shopcart.library.constraints.PriceConstraint;
import common.shopcart.library.constraints.QuantityConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO {

    @NotNull
    private String productName;

    @NotNull
    @QuantityConstraint
    private String quantity;

    private String productDescription;

    private List<String> subType;

    @NotNull
    @PriceConstraint
    private String price;

    private String brand;
}
