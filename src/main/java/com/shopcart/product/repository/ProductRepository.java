package com.shopcart.product.repository;

import com.shopcart.product.models.enums.ProductType;
import com.shopcart.product.repository.model.ProductDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends MongoRepository<ProductDocument, String> {
    Optional<ProductDocument> findByProductNameAndBrand(String productName ,String brand);

    Iterable<ProductDocument> findByType(ProductType type);
}
