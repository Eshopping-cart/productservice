package com.shopcart.product.repository.model;

import com.shopcart.product.models.enums.ProductType;
import common.shopcart.library.constraints.PriceConstraint;
import common.shopcart.library.constraints.QuantityConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "productInfo")
public class ProductDocument {

    @Id
    private String productId;

    @Indexed
	@NotNull
	private String productName;

    @NotNull
	@PriceConstraint
	private String price;

	@Indexed
	private ProductType type;

	@Indexed
	private List<String> subType;

	@Indexed
	private String brand;

	private String productDescription;

	@QuantityConstraint
	private String quantity;
}
